<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => 'web'], function() {

    Route::get('/', function () {
        return redirect('login');
    })->name('main');

    // Authentication routes...
    Route::get('auth/login', 'Auth\AuthController@getLogin');
    Route::post('auth/login', 'Auth\AuthController@postLogin');
    Route::get('auth/logout', 'Auth\AuthController@getLogout');

    // Registration routes...
    Route::get('auth/register', 'Auth\AuthController@getRegister');
    

    // Password reset link request routes...
    Route::get('password/email', 'Auth\PasswordController@getEmail');
    Route::post('password/email', 'Auth\PasswordController@postEmail');

    // Password reset routes...
    Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
    Route::post('password/reset', 'Auth\PasswordController@postReset');

    Route::group(['middleware' => 'auth'], function() {

        Route::get('/user-profile', [
            'uses' => 'UsersController@getProfile',
            'as' => 'profile'
        ]);

        Route::get('/register-client', [
            'uses' => 'ClientsController@getProfile',
            'as' => 'register-client'
        ]);

        Route::get('/delete-client', [
            'uses' => 'ClientsController@deleteClient',
            'as' => 'delete-client'
        ]);

        Route::get('/list-client', [
            'uses' => 'ListClientController@getProfile',
            'as' => 'list.client'
            ]);

        Route::get('/asignar-areas-a-usuario/{userId}', [
            'uses' => 'AddAreasToClientController@getProfile',
            'as' => 'assignAreasToClient'
            ]);

        Route::get('/asignar-horarios/{userId}', [
            'uses' => 'AsignarHorariosController@getProfile',
            'as' => 'areasToClient'
            ]);

        Route::post('/asignar-areas-a-usuario',[
            'uses' => 'AddAreasToClientController@addAreasToClient',
            'as' => 'asignarAreas'
            ]);
        Route::get('/client-profile/{userId}', [
            'uses' => 'ClientsController@getClientProfile',
            'as' => 'profile'
            ]);

        Route::get('/reception-data/{id}', [
            'uses' => 'DataReceptionController@getProfileId',
            'as' => 'reception.data'
        ]);

        Route::post('/reception-data', [
            'uses' => 'DataReceptionController@registerDocuments',
            'as' => 'reception.datas'
        ]);
    
        Route::get('/register-apoderados/{codigo}',[
            'uses' => 'ApoderadosController@getProfile',
            'as' => 'assign.apoderados'
        ]);

        Route::post('/register-apoderados',[
            'uses' => 'ApoderadosController@registerApoderados',
            'as' => 'assign.apoderadosdata'
        ]);
       
        Route::post('/register-user',[
            'uses' => 'UserRegisterController@registerUser',
            'as' => 'user'
        ]);

        Route::get('/register-user', [
            'uses' => 'UserRegisterController@getProfile',
            'as' => 'register-user'
        ]);

        Route::post('/register-enterprise', [
            'uses' => 'ClientsController@registerEnterprise',
            'as' => 'register-enterprise'
        ]);

        Route::post('/register-client', [
            'uses' => 'ClientsController@registerClient',
            'as' => 'register.client'
        ]);

        Route::post('/register-clinica', [
            'uses' => 'ClinicaController@registerClinica',
            'as' => 'register.client'
        ]);

        Route::post('/register-afp', [
            'uses' => 'AFPController@registerAFP',
            'as' => 'register.client'
        ]);

        Route::get('/assigned-patients', [
            'uses' => 'AssignedPatientsController@getProfile',
            'as' => 'assignedPatients'
        ]);

        Route::post('/register-procedure', [
            'uses' => 'ClientsController@registerProcedure',
            'as' => 'procedure',
            'middleware' => 'roles',
            'roles' => ['Administrator']
        ]);

        Route::get('/checkEmail/{email}', [
            'uses' => 'EventController@checkemail',
            'as' => 'verify'
        ]);
        Route::get('/asignar-clinica/{id}', [
            'uses' => 'AsignarCliController@getProfile',
            'as' => 'asignar-clinica'
        ]);
        Route::post('/asignar-clinica', [
            'uses' => 'AsignarCliController@resgisterAsignationClinica',
            'as' => 'fijar-clinica'
        ]);
        Route::get('/ficha-clinica', [
            'uses' => 'FichaClinicaController@getProfile',
            'as' => 'ficha-clinica'
        ]);

        Route::get('/ficha-medica', [
            'uses' => 'FichaClinicaController@getMedic',
            'as' => 'ficha.clinica.medica'
        ]);

        Route::get('/ficha-fisioterapia', [
            'uses' => 'FichaClinicaController@getFisioterapia',
            'as' => 'ficha.clinica.fisioterapia'
        ]);

        Route::get('/ficha-trabajo-social', [
            'uses' => 'FichaClinicaController@getTrabajoSocial',
            'as' => 'ficha.clinica.trabajo.social'
        ]);

        Route::get('/ficha-psicologia', [
            'uses' => 'FichaClinicaController@getPsicologia',
            'as' => 'ficha.clinica.psicologa'
        ]);

    });
    Route::Auth();
    Route::get('/home', [
        'uses' => 'HomeController@index',
        'as' => 'home'
    ]);
});

