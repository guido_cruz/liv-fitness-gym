<html>
    <head>
        <title>App Name - @yield('title')</title>
    </head>
    <body>
        @section('sidebar')
            Section defined in layout. 
        @show

        <div class="container">
            @yield('content')
        </div>
    </body>
</html>