@extends('layouts.app')

@section('title', 'Page Title')

@section('sidebar')
    @parent
    <p>Guido Content...... in child component</p>
@endsection

@section('content')
    <p>This is my body content.</p>
@endsection